package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class CelsiusTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void convertFromFarenheitStandard() throws Exception 
	{
		assertEquals(21, Celsius.convertFromFarenheit(70));
	}
	
	@Test
	public void convertFromFarenheitBoundaryIn() throws Exception 
	{
		assertEquals(12, Celsius.convertFromFarenheit(54));
	}
	
	@Test
	public void convertFromFarenheitBoundaryOut() throws Exception 
	{
		assertNotEquals(13, Celsius.convertFromFarenheit(54));
	}

	@Test
	public void convertFromFarenheitNegative() throws Exception 
	{
		assertNotEquals(15.55, Celsius.convertFromFarenheit(55));
	}
}
